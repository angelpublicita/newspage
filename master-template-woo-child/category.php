<?php



get_header();



?>



 



<div class="entry-category">



    <div class="container">



        <h1><?php the_archive_title(); ?></h1>



        <hr class="divider-subheader">



        <?php if($geniorama['breadcrumbs-on-off']): ?>



                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">



                    <?php



                        if (function_exists('bcn_display')) {



                            bcn_display();



                        }



                    ?>



                </div>



        <?php endif; ?>



    </div>



</div>



 



<section class="content-category mb-5">



    <div class="container">



        <div class="row">



                <!-- ESPACIO PARA BARRA DE FILTROS -->



            <div class="col-12 col-md-8">



            <div class="contenedor-grid category-grid">

    <?php

        $category = get_queried_object();

        $category = $category->term_id;

        $grid_cat = array(

            'post_type' => 'post',

            'posts_per_page' => 3,

            'cat' => $category

        );

        $post_query_p = new WP_Query($grid_cat);

	    if($post_query_p->have_posts() ):

		    while($post_query_p->have_posts() ):

			    $post_query_p->the_post();

			

			    $limit_title = get_the_title();

    ?>



            <div class="item">

                <?php the_post_thumbnail("medium", array('class' => 'img-card-post img-fluid')); ?>

                <a href="<?php echo get_the_permalink(); ?>" class="gradient-grid">

                    <div class="content-grid">

                        <span class="date"><i class="fas fa-clock"></i> <?php echo get_the_date(); ?></span>

                        <h2 class="title-grid"><?php echo mb_strimwidth($limit_title, 0, 42, '...'); ?></h2>

                        <span class="link-grid">LEER MÁS <i class="fas fa-chevron-right"></i></span>

                    </div>

                </a>

            </div>

    <?php 

        wp_reset_query ();

        endwhile;

        endif;

    ?>

    </div>

                

                <?php

                

                    // CAPTURANDO LOS VALORES POR GET

                    

                    



                     if ( $_GET['orderby']) {



                        $orderby = $_GET['orderby'];



                     } else {



                         $orderby = 'date';



                     }



 



                     if ($_GET['order']) {



                        $order = $_GET['order'];



                     } else {



                        $order = "DESC";



                     }

                    

                     $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                     



                     $wp_query = new WP_Query(



                       array(



                         'orderby' => $orderby,



                         'order' => $order,



                         'cat' => get_query_var('cat'),



                         'paged' => $paged



                       )



                     );

                     



                ?>

                <div class='post-filters-cont'>

                    <div class="results">

                        <span>Mostrando 

                        <?php

                        

                        global $wp_query;

                        $number_post = $wp_query->found_posts;



                        $number_page = $wp_query->post_count;

                        if ($number_post > 7){

                            echo $number_page . ' de ' . $number_post . ' resultados';

                        }else{

                            echo ' ' .$number_post . ' resultados';

                        }

                        ?>

                        </span>

                    </div>

                    <form class='post-filters'>

                        <select name="orderby">

                            <option value='date'>Ordenar por fecha</option>

                            <option value='title'>Order por titulo</option>

                            <option value='rand'>Orden aleatorio</option>

                        </select>

                        <select name="order">

                            <option value='DESC'>Descendente</option>

                            <option value='ASC'>Ascendente</option>

                        </select>

                        <button type="submit" class="btn-filter">FILTRAR</button>

                    </form>

                </div>

                <?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>

                

                    <div class="target-grid">



                        <div class="header">



                            <?php 

                                $categories = get_the_category(); 

                                $cat_name = $categories[0]->cat_name;

                                $category_link = reset(get_the_category($post->ID));

                                $category_id = $category_link->cat_ID;

                            ?>



                            <a href="<?php echo get_category_link( $category_id ); ?>" class="category"><?php echo $cat_name; ?></a>



                            <a href="<?php the_permalink(); ?>" class="date"><i class="fas fa-clock"></i> <?php the_date(); ?></a>



                        </div>



                        <a href="<?php the_permalink(); ?>" class="link-target"></a>



                        <div class="img-target">



                            <?php the_post_thumbnail("medium", array('class' => 'img-card-post img-fluid')); ?>



                        </div>



                        <div class="content">



                            <h4 class="title-target"><?php echo mb_strimwidth(get_the_title(), 0, 45, '...') ?></h4>



                            <p class="description"><?php echo mb_strimwidth(get_the_excerpt(), 0, 90, '...') ?></p>



                        </div>



                        <a href="#" class="icon">



                            <i class="fas fa-chevron-right"></i>



                        </a>



                    </div>



                <?php endwhile; endif; ?>



                 



                <div class="pagination-custom">



                    <?php pagination_custom(); ?>



                </div>



            </div>



            <div class="col-12 col-md-4">



                <div class="sidebar-categories">



                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar Categorías')): endif; ?>



                </div>



            </div>



        </div>



    </div>



</section>



 



 



<?php



get_footer();