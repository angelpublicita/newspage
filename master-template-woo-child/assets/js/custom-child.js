let menu_parents = document.querySelectorAll('.bottom-header .menu-item-has-children');

for (let menu_item of menu_parents) {
    menu_item.addEventListener('mouseover', function () {
        let sub_menu = this.querySelector('.sub-menu');

        sub_menu.classList.add('active');

        // sub_menu.style.display = "block";

        this.addEventListener('mouseleave', function () {

            sub_menu.classList.remove('active');
            //sub_menu.style.display = "none";
        })
    })
}


// Jquery

jQuery(document).ready(function ($) {
    $bg_image = $('.box-bg-image').attr('data-bg');
    $('.box-bg-image').css('background-image',`url(${$bg_image})`);

    // Slick carousel variado
    $('.car-01').slick({

        infinite: false,

        speed: 300,

        slidesToScroll: 1,

        slidesToShow: 4,

        vertical: true,

        verticalSwiping: true,

        dots: false,

        autoplay: false,

        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-long-arrow-alt-up"></i></button>',

        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-long-arrow-alt-down"></i></button>',

        responsive: [{

            breakpoint: 1024,

            settings: {

                slidesToScroll: 1,

                infinite: true,

            }

        }, {

            breakpoint: 639,

            settings: {

                slidesToScroll: 1,

                vertical: true,

                verticalSwiping: false,

            }

        }

            // You can unslick at a given breakpoint now by adding:

            // settings: "unslick"

            // instead of a settings object

        ]

    });


    $('.car-02').slick({

        infinite: false,

        speed: 300,

        slidesToShow: 5,

        slidesToScroll: 1,

        vertical: true,

        verticalSwiping: true,

        dots: false,

        autoplay: false,

        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-long-arrow-alt-up"></i></button>',

        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-long-arrow-alt-down"></i></button>',

        responsive: [{

            breakpoint: 1024,

            settings: {

                slidesToShow: 5,

                slidesToScroll: 1,

                infinite: true,

            }

        }, {

            breakpoint: 639,

            settings: {

                slidesToShow: 5,

                slidesToScroll: 1,

                vertical: true,

                verticalSwiping: false,

            }

        }

            // You can unslick at a given breakpoint now by adding:

            // settings: "unslick"

            // instead of a settings object

        ]

    });

    $('.car-columnistas-page').slick({

        infinite: true,

        slidesToShow: 3,

        slidesToScroll: 1,

        centerMode: true,

        centerPadding: '20px',

        autoplay: true,

        autoplaySpeed: 3000,

        speed: 600,

        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-long-arrow-alt-left"></i></button>',

        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-long-arrow-alt-right"></i></button>',

        centerMode: true,

        centerPadding: '140px',

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerPadding: '5px',
                    arrows: false,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerPadding: '5px',
                    arrows: false,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]


    });



    $('.car-columnistas-widget').slick({

        infinite: true,

        arrows: false,

        slidesToShow: 1,

        slidesToScroll: 1,

        centerPadding: '0px',

        autoplay: true,

        autoplaySpeed: 5000,

        speed: 600,

        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-long-arrow-alt-left"></i></button>',

        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-long-arrow-alt-right"></i></button>',

        draggable: false

    });



    $('.car-second').slick({

        infinite: true,

        slidesToShow: 1,

        slidesToScroll: 1,

        arrows: false,

        autoplay: true,

        speed: 600

    });

    $('.car-categories').slick({
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-long-arrow-alt-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-long-arrow-alt-right"></i></button>',
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });


    $(".next.page-numbers").html('<i class="fas fa-caret-right"></i>');

    $(".prev.page-numbers").html('<i class="fas fa-caret-left"></i>');

    $(".widget_archive li a").prepend("<i class='fas fa-folder'></i>");
    $(".sticky-header .box-logo-and-button .box-logo .img-brand").remove();
    $(".sticky-header .box-logo-and-button .box-logo a").prepend("<img src='https://revistalaliga.com/wp-content/uploads/2020/06/logo-blanco.png'>");
    $('#an-06').hide(0);


}); 