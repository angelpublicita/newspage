<?php

get_header();

?>

 

<div class="entry-category">

    <div class="container">

        <h1><?php the_archive_title(); ?></h1>

        <hr class="divider-subheader">

        <?php if($geniorama['breadcrumbs-on-off']): ?>

                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">

                    <?php

                        if (function_exists('bcn_display')) {

                            bcn_display();

                        }

                    ?>

                </div>

        <?php endif; ?>

    </div>

</div>

 

<section class="content-category mb-5">

    <div class="container">

        <div class="row">

                <!-- ESPACIO PARA BARRA DE FILTROS -->

            <div class="col-12 col-md-8">
                <?php

                    // CAPTURANDO LOS VALORES POR GET

 

                     if ( $_GET['orderby']) {

                        $orderby = $_GET['orderby'];

                     } else {

                         $orderby = 'date';

                     }

 

                     if ($_GET['order']) {

                        $order = $_GET['order'];

                     } else {

                        $order = "DESC";

                     }
 

                     $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                     $wp_query = new WP_Query(
                       array(

                        'post_type' => array('post', 'podcast'),

                         'orderby' => $orderby,

                         'order' => $order,

                         'cat' => get_query_var('cat'),

                         'paged' => $paged

                       )

                     );

                ?>
            
                <div class='post-filters-cont'>
                    <div class="results">
                        <span>Mostrando 
                            <?php
                            global $wp_query;
                            $number_post = $wp_query->found_posts;
                            $number_page = $wp_query->post_count;

                            if ($number_post > 7){

                                echo $number_page . ' de ' . $number_post . ' resultados';

                            }else{

                                echo ' ' .$number_post . ' resultados';

                            }

                            ?>
                        </span>
                    </div>
                    <form class='post-filters'>
                        <select name="orderby">
                            <option value='date'>Ordenar por fecha</option>
                            <option value='title'>Order por titulo</option>
                            <option value='rand'>Orden aleatorio</option>
                        </select>
                        <select name="order">
                            <option value='DESC'>Descendente</option>
                            <option value='ASC'>Ascendente</option>
                        </select>
                        <button type="submit" class="btn-filter">FILTRAR</button>
                    </form>
                </div>
				<?php 
				
				$grid_arch = array(
					'post_type' => array( 'post', 'podcast'),
					'posts_per_page' => 3,
				);
				
				?>

                <?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>

                    <div class="target-grid <?php if(get_post_type() == "podcast"):?> mt-category-podcast <?php endif; ?>">

                        <div class="header">

                            <?php 
                                if (get_post_type() == "podcast") {
                                    $categories = get_the_terms($post->ID, 'categoria-podcast'); 
                                    $cat_name = $categories[0]->name;
                                    $term_link = get_term_link( $cat_name, 'categoria-podcast');
                                } else {
                                    $categories = get_the_category(); 
                                    $cat_name = $categories[0]->cat_name;
                                    $category_link = reset(get_the_category($post->ID));
                                    $category_id = $category_link->cat_ID;
                                }
                            ?>

                            <?php if(get_post_type() == "podcast"): ?>
                                <a href="<?php echo $term_link; ?>" class="category"><?php echo $cat_name; ?></a>
                            <?php else: ?>
                                <a href="<?php echo get_category_link( $category_id ); ?>" class="category"><?php echo $cat_name; ?></a>
                            <?php endif; ?>

                            <a href="<?php the_permalink(); ?>" class="date"><i class="fas fa-clock"></i> <?php the_date(); ?></a>

                        </div>

                        <a href="<?php the_permalink(); ?>" class="link-target"></a>

                        <div class="img-target position-relative">

                            <?php the_post_thumbnail("full", array('class' => 'img-card-post img-fluid')); ?>
                            <?php if(get_post_type() == "podcast"): ?>
                                <a href="<?php echo get_the_permalink(); ?>" class="caption-target-podcast d-flex justify-content-center align-items-center position-absolute w-100"><i class="fas fa-microphone"></i></a>
                            <?php endif; ?>

                        </div>

                        <div class="content">
                            <?php if(get_post_type() == "podcast"): ?>
                                <span class='name-post-type'>PODCAST</span>
                            <?php endif; ?>

                            <h4 class="title-target"><?php echo mb_strimwidth(get_the_title(), 0, 45, '...') ?></h4>

                            <?php if(get_post_type() == "podcast" && get_field('duracion')): ?>
                                <span class="duration-podcast"><span class="head-duration">Duración: </span> <?php the_field('duracion'); ?></span>
                            <?php else: ?>
                                <p class="description"><?php echo mb_strimwidth(get_the_excerpt(), 0, 90, '...') ?></p>
                            <?php endif; ?>

                        </div>

                        <a href="#" class="icon">

                            <i class="fas fa-chevron-right"></i>

                        </a>

                    </div>

                <?php endwhile; endif; ?>

 

                <div class="pagination-custom">

                    <?php pagination_custom(); ?>

                </div>

            </div>

            <div class="col-12 col-md-4">

                <div class="sidebar-categories">

                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar Categorías')): endif; ?>

                </div>

            </div>

        </div>

    </div>

</section>

 

 

<?php

get_footer();
