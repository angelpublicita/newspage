<?php

function enqueue_styles_child_theme() {

	$parent_style = 'master-template-woo-style';
	$child_style  = 'master-template-woo-child-style';

	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'slick-css', get_stylesheet_directory_uri() . '/assets/css/slick.css', '4.1' );
	wp_enqueue_style( $child_style, get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version'));
	wp_enqueue_style( 'custom-style-css', get_stylesheet_directory_uri() . '/assets/css/custom-style.css', '1.0' );
	wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/assets/js/slick.min.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'custom-child-js', get_stylesheet_directory_uri() . '/assets/js/custom-child.js', array('jquery'), '1.0', true);
}

add_action( 'wp_enqueue_scripts', 'enqueue_styles_child_theme' );

//Funciones de ayuda
require_once "inc/helpers.php";

//SHORTCODES
require_once "inc/subMenu.php";
require_once "inc/shortcodes/sc_grid_principal_home.php";
require_once "inc/shortcodes/sc_otras_noticias.php";
require_once "inc/shortcodes/sc_lo_mas_leido_home.php";
require_once "inc/shortcodes/sc_carousel_author.php";
require_once "inc/shortcodes/sc_social_icons.php";
require_once "inc/shortcodes/sc_grid_home_second.php";
require_once "inc/shortcodes/sc_ediciones_impresas_footer.php";
require_once "inc/shortcodes/sc_podcast_destacado.php";
require_once "inc/shortcodes/sc_carousel_categories.php";

//WIDGETS
require_once "inc/widgets/wg_mas_del_autor.php";
require_once "inc/widgets/wg_lo_mas_leido.php";
require_once "inc/widgets/wg_columnistas.php";
require_once "inc/widgets/wg_mas_noticias.php";
require_once "inc/widgets/wg_categorias_destacadas.php";
require_once "inc/widgets/wg_categorias_podcast.php";
require_once "inc/widgets/wg_mas_podcast.php";

//CUSTOM POST TYPES
require_once "inc/cpt/cpt_ediciones_impresas.php";
require_once "inc/cpt/cpt_podcast.php";

//Add custom post types
function add_my_post_types_to_query( $query ) {
	if ( (is_single() || is_home() || is_category() ) && $query->is_main_query() )
		$query->set( 'post_type', array( 'post', 'podcast', 'ediciones' ) );

	return $query;
}

//CUSTOM TAXONOMIES
require_once "inc/cpt/tax_podcast_cat.php";


// Añadir campos contacto a perfiles columnistas
add_filter('user_contactmethods','agregar_campos_contacto');

function agregar_campos_contacto( $arr ) {
    $arr['rol'] = __('Rol');
	$arr['facebook'] = __('Facebook');
	$arr['twitter'] = __('Twitter');
	$arr['instagram'] = __('Instagram');
    return $arr;
}





