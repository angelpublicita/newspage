<?php

//SHORTCODE PODCAST DESTACADO
if(!function_exists('item_star_func')){
    function item_star_func($atts){
        $atts = shortcode_atts(
            array(
                'post_type' => 'podcast',
                'tax_query' => array(
                    array(
                        'taxonomy'  => 'categoria-podcast',
                        'field'     => 'slug',
                        'terms'     => array('podcast-destacado')
                    ),
                ),
                'post_per_page' => 1
            ),
            $atts,
            'item-star'
        );

        $post_query = new WP_Query($atts);

        if($post_query->have_posts()){
            while ($post_query->have_posts()) {
                $post_query->the_post();

                $string = '<div class="item-star position-relative">';
                $string .= get_the_post_thumbnail( null, 'full', array('class' => 'img-fluid img-item-star') );
                $string .= '<a href="'.get_the_permalink().'" class="caption d-flex align-items-center position-absolute p-4 justify-content-between">';
                $string .= '<div class="info">';
                $string .= '<span class="name-post-type text-uppercase">'.get_post_type(get_the_ID()).'</span> | ';
                $string .= '<span class="date"><i class="fas fa-clock"></i> '.get_the_date().'</span>';
                $string .= '<h4 class="title my-2">'.mb_strimwidth(get_the_title(), 0, 55, '...').'</h4>';
                $string .= '</div>'; //Fin info
                $string .= '<div class="item-icon p-2">';
                $string .= '<i class="fas fa-microphone"></i>';
                $string .= '</div>'; //Fin Item icon
                $string .= '</a>'; //Fin caption
                $string .= '</div>'; //Fin Item star

                return $string;
            }
        }
    }
}

add_shortcode('item_star', 'item_star_func' );