<?php

// Shortcode Grid Home
function grid_home_shortcode() {
	
	// Container grid
	$cont = '<div class="contenedor-grid">';

	// Loop entrada destacada
	$grid_p = array(
		'post_type' => 'post',
		'posts_per_page' => 1,
		'category_name' => 'Entrada destacada'
	);
	$post_query_p = new WP_Query($grid_p);
	if($post_query_p->have_posts() ) {
		while($post_query_p->have_posts() ) {
			$post_query_p->the_post();
			
			$limit_title = get_the_title();

			// Principal grid items
			$cont .= '<div class="principal-item item">';
			$cont .= get_the_post_thumbnail($post = null, $size = "post-full", $attr = "class=img-card-post img-fluid");
			$cont .= '<a href="'.get_the_permalink().'" class="gradient-grid">';
			$cont .= '<div class="content-grid">';
			$cont .= '<span class="date"><i class="fas fa-clock"></i> '.get_the_date().'</span>';
			$cont .= '<h2 class="title-grid">'.mb_strimwidth($limit_title, 0, 60, '...').'</h2>';
			$cont .= '<span class="link-grid">LEER MÁS <i class="fas fa-chevron-right"></i></span>';
			$cont .= '</div>';
			$cont .= '</a>';
			$cont .= '</div>';
		}
	}


	// Loop ultimas entradas
	$args = array(
		'post_type' => 'post',
		'category__not_in' => array( 16 ),
		'posts_per_page' => 4,
        'tax_query' => array(
        array(
            'taxonomy' => 'tipo_publicacion',
            'field'    => 'slug',
            'terms'    => 'noticia',
        ),
    ),
	);
	$post_query = new WP_Query($args);
	if($post_query->have_posts() ) {
		while($post_query->have_posts() ) {
			$post_query->the_post();
			
			$limit_title = get_the_title();

			// Item
			
			$cont .= '<div class="item item-secondary">';
			$cont .= get_the_post_thumbnail($post = null, $size = "post-full", $attr = "class=img-card-post img-fluid");
			$cont .= '<a href="'.get_the_permalink().'" class="gradient-grid">';
			$cont .= '<div class="content-grid">';
			$cont .= '<span class="date"><i class="fas fa-clock"></i> '.get_the_date().'</span>';
			$cont .= '<h2 class="title-grid">'.mb_strimwidth($limit_title, 0, 42, '...').'</h2>';
			$cont .= '<span class="link-grid">LEER MÁS <i class="fas fa-chevron-right"></i></span>';
			$cont .= '</div>';
			$cont .= '</a>';
			$cont .= '</div>';
		}
	}

	// End contenedor grid
	$cont .= '</div>';

	wp_reset_query ();
	return $cont;
}
add_shortcode( 'grid_home', 'grid_home_shortcode' );