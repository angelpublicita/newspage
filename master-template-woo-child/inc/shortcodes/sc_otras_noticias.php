<?php

// Shorcode otras noticias
function carousel_var_shortcode($atts) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'content' => 'date',
			'ad-number' => '',
			'show-ad' => false,
			'post-type' => 'post',
			'post-per-page' => 13,
			'post-show' => 4
		),
		$atts,
		'carousel_var'
	);

	$c_var = '';
	
	$c_var .= '<div class="slider-vertical car-01" data-slick={"slidesToShow":'.$atts["post-show"].'}>';

	if ($atts['show-ad']) {
		echo do_shortcode('[adrotate banner="'.$atts["ad-number"].'"]');
	}

	if($atts['post-type'] == "post"){
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => $atts['post-per-page'],
			'orderby' => $atts['content'],
			//Excluye categoría
			'category__not_in' => $exclude_cats,
			'tax_query' => array(
			array(
				'taxonomy' => 'tipo_publicacion',
				'field'    => 'slug',
				'terms'    => 'noticia',
				),
			),
		);
		$class_container = "mt-category-post";
	} elseif($atts['post-type'] == "podcast"){
		$args = array(
			'post_type' => 'podcast',
			'posts_per_page' => $atts['post-per-page'],
			'orderby' => $atts['content'],
		);
		$class_container = "mt-category-podcast";
	}
	
	$post_query = new WP_Query($args);
	if($post_query->have_posts() ) {
		$contador = 0;
		while($post_query->have_posts() ) {
			$post_query->the_post();
			$contador = $contador + 1;
			
			if ($contador < 5 && $atts['post-type'] == "post") {
				continue;
			}
			
			$limit_title = get_the_title();
			$limit_content = get_the_excerpt();

			// item
			
			$c_var .= '<div class="target-grid '.$class_container.'">';
			$c_var .= '<div class="header">';

			if ($atts['post-type'] == "podcast") {
				$categories = get_the_terms($post->ID, 'categoria-podcast'); 
				$cat_name = $categories[0]->name;
				$term_link = get_term_link( $cat_name, 'categoria-podcast');
			} else {
				$categories = get_the_category(); 
				$cat_name = $categories[0]->cat_name;
				$category_link = reset(get_the_category($post->ID));
				$category_id = $category_link->cat_ID;
			}
			
			
			if($atts['post-type'] == "podcast"){
				$c_var .= '<a href="'.$term_link.'" class="category">'.$cat_name.'</a>';
			} else {
				$c_var .= '<a href="'.get_category_link( $category_id ).'" class="category">'.$cat_name.'</a>';
			}
			
			
			$c_var .= '<a href="'.get_the_permalink().'" class="date"><i class="fas fa-clock"></i> '.get_the_date().'</a>';
			$c_var .= '</div>';
			$c_var .= '<a href="'.get_the_permalink().'" class="link-target"></a>';
			$c_var .= '<div class="img-target position-relative">';
			$c_var .= get_the_post_thumbnail($post = null, $size = "medium", $attr = "class=img-card-post img-fluid");
			if($atts['post-type'] == "podcast"){
				$c_var .= '<a href="'.get_the_permalink().'" class="caption-target-podcast d-flex justify-content-center align-items-center position-absolute w-100"><i class="fas fa-microphone"></i></a>';
			}
			$c_var .= '</div>';
			$c_var .= '<div class="content">';
			
			// Titulo
			if($atts['post-type'] == "podcast"){
				$c_var .= '<span class="name-post-type">PODCAST</span>';
				$c_var .= '<h4 class="title-target">'.mb_strimwidth($limit_title, 0, 50, '...').'</h4>';
				if(get_field('duracion')){
					$c_var .= '<span class="duration-podcast"><span class="head-duration">Duración: </span>'.get_field('duracion').'</span>';
				}
			} else {
				$c_var .= '<h4 class="title-target">'.mb_strimwidth($limit_title, 0, 45, '...').'</h4>';
			}

			//Descripción
			if($atts['post-type'] == "post"){
				$c_var .= '<p class="description">'.mb_strimwidth($limit_content, 0, 55, '...').'</p>';
			}
			$c_var .= '</div>';
			$c_var .= '<a href="#" class="icon">';
			$c_var .= '<i class="fas fa-chevron-right"></i>';
			$c_var .= '</a>';
			$c_var .= '</div>';
		}
	}

	wp_reset_query ();
	$c_var .= '</div>';

	return $c_var;
}
add_shortcode( 'carousel_var', 'carousel_var_shortcode' );