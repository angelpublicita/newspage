<?php



// Shorcode carousel contador variado
function carousel_var_cont_shortcode() {
	$week = date('W');
	$year = date('Y');

	$c_var = '';
	$count = 0;
	
	$args = array(
	    'post_type' => 'post',
		'posts_per_page' => 5,
		'meta_query' => array(
			'relation' => 'AND',
			'query_one' => array(
				'key' => 'post_views'
			)
		),
        'orderby' => array(
			'query_one' => 'DESC',
			'date' => 'DESC'
		),
		'date_query' => array(
			array(
				'column' => 'post_date_gmt',
				'after'  => '7 days ago',
			)
		),
	);
	
	$post_query = new WP_Query($args);
	if($post_query->have_posts() ) {
		$c_var .= '<div class="slider-vertical car-02">';
		while($post_query->have_posts() ) {
			$post_query->the_post();
			$count++;
			
			$limit_title = get_the_title();
			$limit_content = get_the_excerpt();

			// item
			$c_var .= '<div class="target-grid">';
			$c_var .= '<div class="header">';
			$categories = get_the_category(); 
			$cat_name = $categories[0]->cat_name;
			$category_link = reset(get_the_category($post->ID));
			$category_id = $category_link->cat_ID;
			
			$c_var .= '<a href="'.get_category_link( $category_id ).'" class="category">'.$cat_name.'</a>';
			
			$c_var .= '<a href="'.get_the_permalink().'" class="date"><i class="fas fa-clock"></i> '.get_the_date().'</a>';
			$c_var .= '</div>';
			$c_var .= '<a href="'.get_the_permalink().'" class="link-target"></a>';
			$c_var .= '<span class="cont-post">';
			if ($count<6){
				$c_var .= $count;
			}

			$c_var .= '</span>';
			$c_var .= '<div class="img-target">';
			$c_var .= get_the_post_thumbnail($post = null, $size = "medium", $attr = "class=img-card-post img-fluid");
			$c_var .= '</div>';
			$c_var .= '<div class="content">';
			$c_var .= '<h4 class="title-target">'.mb_strimwidth($limit_title, 0, 45, '...').'</h4>';
			$c_var .= '<p class="description">'.mb_strimwidth($limit_content, 0, 55, '...').'</p>';
			$c_var .= '</div>';
			$c_var .= '<a href="#" class="icon">';
			$c_var .= '<i class="fas fa-chevron-right"></i>';
			$c_var .= '</a>';
			$c_var .= '</div>';
		}
	}

	$c_var .= '</div>';
    wp_reset_query ();
	return $c_var;
}
add_shortcode( 'carousel_cont', 'carousel_var_cont_shortcode' );