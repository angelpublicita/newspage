<?php

// Shortcode Grid Home
function grid_home_second_shortcode() {
	
	// Container grid
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 6,
		'orderby' => 'comment_count',
		'cat' => 23
	);
	$post_query = new WP_Query($args);
	if($post_query->have_posts() ) {
		$conta = '<div class="contenedor-grid grid-second">';
	    while($post_query->have_posts() ) {
			$post_query->the_post();

			$limit_title = get_the_title();
			$limit_content = get_the_excerpt();
			$categories = get_the_category();
			

			$conta .= '<div class="item">';
			
			$conta .= get_the_post_thumbnail($post = null, $size = "full", $attr = "class=img-card-post img-fluid");
			$conta .= '<a href="'.get_the_permalink().'" class="gradient-grid">';
			$conta .= '<div class="content-grid">';
			
			$conta .= '<h2 class="title-grid">'.mb_strimwidth($limit_title, 0, 42, '...').'</h2>';
			$conta .= '<p class="text-grid">'.mb_strimwidth($limit_content, 0, 75, '...').'</p>';
			
			$conta .= '<span class="link-grid">LEER MÁS <i class="fas fa-chevron-right"></i></span>';
			$conta .= '</div>';
			$conta .= '</a>';
			$conta .= '<ul class="nav justify-content-center social-newslatter">';
			$conta .= '<li class="nav-item"><a href="https://www.facebook.com/sharer/sharer.php?u='.get_the_permalink().'" class="button-icon" target="_blank"><i class="fab fa-facebook-square"></i></a></li>';
			$conta .= '<li class="nav-item"><a href="https://twitter.com/home?status='.get_the_permalink().'"  class="button-icon" target="_blank"><i class="fab fa-twitter-square"></i></a></li>';
			$conta .= '<li class="nav-item"><a href="https://pinterest.com/pin/create/button/?url='.get_the_permalink().'&media=&description=" class="button-icon" target="_blank"><i class="fab fa-pinterest" target="_blank"></i></a></li>';
			$conta .= '</ul>';
			$categories = get_the_category(); 
			$cat_name = $categories[0]->cat_name;
			$category_link = reset(get_the_category($post->ID));
			$category_id = $category_link->cat_ID;
			
				$conta .= '<a href="'.get_category_link( $category_id ).'" class="category-grid">'.$cat_name.'</a>';
		
			$conta .= '</div>';
		}
	}		

	// End contenedor grid
	$conta .= '</div>';

	wp_reset_query ();
	return $conta;
}
add_shortcode( 'grid_home_second', 'grid_home_second_shortcode' );