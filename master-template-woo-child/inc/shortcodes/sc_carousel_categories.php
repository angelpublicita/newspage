<?php

if(!function_exists("carousel_categories_func")){
    function carousel_categories_func($atts){
        $atts = shortcode_atts( 
            array(
                "post_type" => "post"

            ),
            $atts,
            "carousel_categories"
        );

        $terms = get_terms(array('taxonomy' => 'categoria-podcast'));

        $string = '<div class="car-categories" data-slick={"slidesToShow":4}>';
        foreach ($terms as $term) {
            $string .= '<div class="item-cat position-relative">';
            $string .= '<img src='.z_taxonomy_image_url($term->term_id, "medium").' class="img-cat">';
            $string .= '<a href='.get_term_link($term->name, "categoria-podcast").' class="cat-caption d-flex justify-content-center align-items-center flex-column position-absolute">';
            $string .= '<img src="'.get_stylesheet_directory_uri().'/assets/img/podcast-resources/sound-waves-1.png" class="position-absolute sound-waves">';
            $string .= '<h5>'.$term->name.'</h5>';
            $string .= '</a>';
            $string .= '</div>';
        }
        $string .= '</div>';

        return $string;
    }

    add_shortcode("carousel_categories", "carousel_categories_func");
}