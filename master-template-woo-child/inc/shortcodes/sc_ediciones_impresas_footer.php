<?php
//SHORTCODE EDICIONES FOOTER
function ediciones_portadas_func(){
	
	$terms = get_terms( array(
        'taxonomy'   => 'nombre_edicion', // Swap in your custom taxonomy name
        'orderby' => 'meta_value',
        'order' => 'DESC',
    ));

    $number_of_taxonimy = 8;
    $terms = array_slice($terms, 0, $number_of_taxonimy, true);
    echo '<h4 class="widget-title mb-4">EDICIONES REVISTA LA LIGA</h4>';
    echo '<div class="edition-footer row">';
    // Loop through all terms with a foreach loop
    foreach( $terms as $term ) {
        $tax = '';
        $tax .= '<a href="'. get_term_link( $term ) .'" class="item-edition col-md-3 col-4">';
        $tax .= '<img src="'.z_taxonomy_image_url($term->term_id, 'thumbnail').'">';
        $tax .= '<span>'. $term->name .'</span>';
        $tax .= '</a>';
        
        echo $tax;
    }

    echo '</div>';

}

add_shortcode('ediciones_portadas', 'ediciones_portadas_func');