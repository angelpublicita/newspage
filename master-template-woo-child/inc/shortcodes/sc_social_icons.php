<?php

// Social buttons
function show_social_buttons_custom($atts){
    global $geniorama;
    
    $atts = shortcode_atts(
        array(
            'clase-css' => '',
            'clase-contenedor' => ''
        ),
        $atts,
        "show_social_buttons"
    );

	?>
    <ul class="social-icons nav <?php echo $atts['clase-contenedor']; ?>">
        <?php if ($geniorama['float-fb-button']): ?>
        <!-- Facebook -->
            <li class="nav-item">
                <a href="<?php echo $geniorama['social-fb'];?>" class="square-link link-facebook <?php echo $atts['clase-css']; ?>" target="_blank">
                    <i class="fab fa-facebook-square"></i>
                </a>
            </li>
        <?php endif; ?>

        <?php if ($geniorama['float-ins-button']): ?>
        <!-- Instagram -->
            <li class="nav-item">
                <a href="<?php echo $geniorama['social-ins']; ?>" class="square-link link-instagram <?php echo $atts['clase-css']; ?>" target="_blank">
                    <i class="fab fa-instagram-square"></i>
                </a>
            </li>
        <?php endif; ?>

        <?php if ($geniorama['float-yt-button'] == '1'): ?>
        <!-- YouTube -->
            <li class="nav-item">
                <a href="<?php echo $geniorama['social-yt']; ?>" class="square-link link-youtube <?php echo $atts['clase-css']; ?>" target="_blank">
                    <i class="fab fa-youtube-square"></i>
                </a>
            </li>
        <?php endif; ?>

        <?php if ($geniorama['float-tw-button']): ?>
        <!-- Twitter -->
        <li class="nav-item">
            <a href="<?php echo $geniorama['social-tw']; ?>" class="square-link link-twitter <?php echo $atts['clase-css']; ?>" target="_blank">
                <i class="fab fa-twitter-square"></i>
            </a>
        </li>
        <?php endif; ?>

        <?php if ($geniorama['float-li-button']): ?>
            <!-- Linked In -->
            <li class="nav-item">
                <a href="<?php echo $geniorama['social-li']; ?>" class="square-link link-linkedin <?php echo $atts['clase-css']; ?>" target="_blank">
                    <i class="fab fa-linkedin"></i>
                </a>
            </li>
        <?php endif; ?>

        <?php if ($geniorama['float-pt-button']): ?>
            <!-- Pinterest -->
            <li class="nav-item">
                <a href="<?php echo $geniorama['social-pt']; ?>" class="square-link link-pinterest <?php echo $atts['clase-css']; ?>" target="_blank">
                    <i class="fab fa-pinterest-square"></i>
                </a>
            </li>
        <?php endif; ?>

        <?php if($geniorama['opt-whp']): ?>
            <!-- Whatsapp -->
            <li class="nav-item">
                <a href="<?php echo api_whatsapp(); ?>" class="<?php echo $atts['clase-css']; ?>" target="_blank">
                    <i class="fab fa-whatsapp-square"></i>
                </a>
            </li>
        <?php endif; ?>
    </ul>
	<?php
}

add_shortcode("show_social_buttons", "show_social_buttons_custom");