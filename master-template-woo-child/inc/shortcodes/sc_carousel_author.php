<?php

// Shorcode Columnistas
function carousel_column_shortcode($atts) {
	global $current_user;

	$atts = shortcode_atts(
		array(
			'post-type' => 'post',
			'id'		=> '',
			'show-items' => 1
		),
		$atts,
		'carousel_column'
	);

	$colum = '';

	$args = array(
		'role' => 'Author',
		'has_published_posts' => $atts['post-type']
	);

	if($atts['id']){
		$unique_id = "id=" . $atts['id'];
	}
	
	// The Query
	$user_query = new WP_User_Query( $args );
	get_currentuserinfo(); 
	// User Loop
	if ( ! empty( $user_query->get_results() ) ) {
		$colum .= '<div class="car-columnistas car-columnistas-page" '.$unique_id.' data-slick={"slidesToShow":'.$atts['show-items'].'}>';
		foreach ( $user_query->get_results() as $user ) {
			$id_user = $user->ID;
			$author_ID = get_query_var('author');
			$colum .= '<div class="column-item">';
			$colum .= '<div class="cont-img-column">';
			$colum .= '<img class="img-column" src="'.get_avatar_url($id_user, array( "size" => 400 )).'">';
			$colum .= '</div>';
			$colum .= '<a href="'.esc_url( get_author_posts_url( get_the_author_meta( 'ID', $user->ID ) ) ).'"><h3 class="title-author">' . $user->display_name . '</h3></a>';
			$colum .= '<span class="role">'.$user->rol.'</span>';
			
			$colum .= '<div class="car-second">';
			$args = array(
				'post_type' => $atts['post-type'],
				'author' => $id_user, 
				'posts_per_page' => 1,
			);
			$post_query = new WP_Query($args);
			if($post_query->have_posts() ) {
				while($post_query->have_posts() ) {
					$post_query->the_post();
					$lim_title = get_the_title();
					$colum .= '<div class="item row">';
					$colum .= '<a href="'.get_the_permalink().'" class="link"></a>';
					$colum .= '<div class="cont-img col-3">';
					$colum .= get_the_post_thumbnail($post = null, $size = "thumbnail", $attr = "class=img-card-post img-fluid");
					$colum .= '</div>';
					$colum .= '<div class="cont-text col-9">';
					$colum .= '<span class="date"><i class="fas fa-clock"></i> '.get_the_date().'</span>';
					$colum .= '<h4 class="title-notice">'.mb_strimwidth($lim_title, 0, 40, '...').'</h4>';
					$colum .= '</div>';
					$colum .= '</div>';
				}
			}
			$colum .= '</div>';	
			$colum .= '</div>';
		}
	} else {
		echo 'No hay autores';
	}

    
	$colum .= '</div>';

	wp_reset_query ();
	return $colum;
}
add_shortcode( 'carousel_column', 'carousel_column_shortcode' );