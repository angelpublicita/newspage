<?php

if ( ! function_exists('wpn_ediciones_impresas') ) {

    // Register Custom Post Type
    function wpn_ediciones_impresas() {
                
        $labels = array(
            'name'                  => _x( 'Ediciones', 'Post Type General Name', 'text_domain' ),
            'singular_name'         => _x( 'Edicion', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'             => __( 'Ediciones Impresas', 'text_domain' ),
            'name_admin_bar'        => __( 'Ediciones Impresas', 'text_domain' ),
            'archives'              => __( 'Archivo de ediciones', 'text_domain' ),
            'attributes'            => __( 'Atributos de edición', 'text_domain' ),
            'parent_item_colon'     => __( 'Edición padre', 'text_domain' ),
            'all_items'             => __( 'Todas la ediciones', 'text_domain' ),
            'add_new_item'          => __( 'Añadir nueva edición', 'text_domain' ),
            'add_new'               => __( 'Añadir nuevo', 'text_domain' ),
            'new_item'              => __( 'Nueva edición', 'text_domain' ),
            'edit_item'             => __( 'Editar edición', 'text_domain' ),
            'update_item'           => __( 'Actualizar edición', 'text_domain' ),
            'view_item'             => __( 'Ver edición', 'text_domain' ),
            'view_items'            => __( 'Ver ediciones', 'text_domain' ),
            'search_items'          => __( 'Buscar edición', 'text_domain' ),
            'not_found'             => __( 'No encontrado', 'text_domain' ),
            'not_found_in_trash'    => __( 'No encontrado en la papelera', 'text_domain' ),
            'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
            'set_featured_image'    => __( 'Configurar imagen destacada', 'text_domain' ),
            'remove_featured_image' => __( 'Borrar imagen destacada', 'text_domain' ),
            'use_featured_image'    => __( 'Usar como imagen destacada', 'text_domain' ),
            'insert_into_item'      => __( 'Insertar en la edición', 'text_domain' ),
            'uploaded_to_this_item' => __( 'Actualizar en esta edición', 'text_domain' ),
            'items_list'            => __( 'Listado de ediciones', 'text_domain' ),
            'items_list_navigation' => __( 'Lista navegable de ediciones', 'text_domain' ),
            'filter_items_list'     => __( 'Filtro de lista de ediciones', 'text_domain' ),
        );
        $args = array(
            'label'                 => __( 'Edicion', 'text_domain' ),
            'description'           => __( 'Entradas de ediciones', 'text_domain' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
            'taxonomies'            => array( 'category', 'post_tag' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-format-aside',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'ediciones', $args );
                
        }
        add_action( 'init', 'wpn_ediciones_impresas', 0 );
                
}