<?php

// Widget Columnistas
class wpb_widget_03 extends WP_Widget {
  
    function __construct() {
    parent::__construct(
      
    // Base ID of your widget
    'wpb_widget_03', 
      
    // Widget name will appear in UI
    __('Columnistas', 'wpb_widget_domain_03'), 
      
    // Widget description
    array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain_03' ), ) 
    );
    }
      
    // Creating widget front-end
      
    public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
      
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
    echo $args['before_title'] . $title . $args['after_title'];
      
    // This is where you run the code and display the output
    global $current_user;
    $colum = '';

    $args = array(
        'role' => 'Author'
        // nombre- > dinamico
        // limite de noticias
    );
    
    // The Query
    $user_query = new WP_User_Query( $args );
    get_currentuserinfo(); 
    // User Loop
    if ( ! empty( $user_query->get_results() ) ) {
        $colum .= '<div class="car-columnistas car-columnistas-widget">';
        foreach ( $user_query->get_results() as $user ) {
            $id_user = $user->ID;
            $author_ID = get_query_var('author');
            $colum .= '<div class="column-item">';
            $colum .= '<div class="cont-img-column">';
            $colum .= '<img class="img-column" src="'.get_avatar_url($id_user, array( "size" => 400 )).'">';
            $colum .= '</div>';
            $colum .= '<a href="'.esc_url( get_author_posts_url( get_the_author_meta( 'ID', $user->ID ) ) ).'"><h3 class="title-author">' . $user->display_name . '</h3></a>';
            $colum .= '<span class="role">'.$user->rol.'</span>';
            
            $colum .= '<div class="car-second">';
            $args = array(
                'post_type' => 'post',
                'author' => $id_user, 
                'posts_per_page' => 1,
            );
            $post_query = new WP_Query($args);
            if($post_query->have_posts() ) {
                while($post_query->have_posts() ) {
                    $post_query->the_post();
                    $lim_title = get_the_title();
                    $colum .= '<div class="item row">';
                    $colum .= '<a href="'.get_the_permalink().'" class="link"></a>';
                    $colum .= '<div class="cont-img col-2">';
                    $colum .= get_the_post_thumbnail($post = null, $size = "post-full", $attr = "class=img-card-post img-fluid");
                    $colum .= '</div>';
                    $colum .= '<div class="cont-text col-10">';
                    $colum .= '<span class="date"><i class="fas fa-clock"></i> '.get_the_date().'</span>';
                    $colum .= '<h4 class="title-notice">'.mb_strimwidth($lim_title, 0, 30, '...').'</h4>';
                    $colum .= '</div>';
                    $colum .= '</div>';
                    

                    
                }
            }
            $colum .= '</div>';	
            $colum .= '</div>';
        }
    } else {
        echo 'No users found.';
    }

    
    $colum .= '</div>';

    wp_reset_query ();

    echo $colum;
    echo $args['after_widget'];
    }
              
    // Widget Backend 
    public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
    $title = $instance[ 'title' ];
    }
    else {
    $title = __( 'New title', 'wpb_widget_domain_03' );
    }
    // Widget admin form
    ?>
    <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php 
    }
          
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
    }
     
    // Class wpb_widget ends here
} 
     
     
// Register and load the widget
function wpb_load_widget_03() {
    register_widget( 'wpb_widget_03' );
}
add_action( 'widgets_init', 'wpb_load_widget_03' );