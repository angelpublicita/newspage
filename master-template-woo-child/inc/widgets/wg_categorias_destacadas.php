<?php

class wpb_widget_05 extends WP_Widget {
  
    function __construct() {
    parent::__construct(
      
    // Base ID of your widget
    'wpb_widget_05', 
      
    // Widget name will appear in UI
    __('Categorías Destacadas', 'wpb_widget_domain_05'), 
      
    // Widget description
    array( 'description' => __( 'Widget para mostrar categorías', 'wpb_widget_domain_05' ), ) 
    );
    }
      
    // Creating widget front-end
      
    public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
      
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
    echo $args['before_title'] . $title . $args['after_title'];
      
    // This is where you run the code and display the output
 
    
    //for each category, show 5 posts
    $categoria = get_query_var('cat' );
    $cat_args=array(
          'orderby' => 'front_order_id',
        'order' => 'ASC',
        'parent'       => 0,
        'exclude' => array($categoria,16,1)
    );
       
    $categories=get_categories($cat_args);
    $number_of_categories = 3;
    $categories = array_slice($categories, 0, $number_of_categories, true);
      foreach($categories as $category) { 
        if($category->name !== $categoria){
            echo '<div class="widget-img-cat">
                    <img src="'.z_taxonomy_image_url($category->term_id, 'medium').'">
                      <a href="' . get_category_link( $category->term_id ) . '" class="link-category" >' . $category->name.'</a> 
                  </div>';
        }

    } // foreach($categories

    echo $args['after_widget'];
    }
              
    // Widget Backend 
    public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
    $title = $instance[ 'title' ];
    }
    else {
    $title = __( 'New title', 'wpb_widget_domain_05' );
    }
    // Widget admin form
    ?>
    <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php 
    }
          
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
    }
     
    // Class wpb_widget ends here
    } 
     
 
    // Register and load the widget
    function wpb_load_widget_05() {
        register_widget( 'wpb_widget_05' );
    }
    add_action( 'widgets_init', 'wpb_load_widget_05' );