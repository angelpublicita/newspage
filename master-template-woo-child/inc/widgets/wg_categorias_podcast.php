<?php

class wpb_widget_06 extends WP_Widget {
  
    function __construct() {
    parent::__construct(
      
    // Base ID of your widget
    'wpb_widget_06', 
      
    // Widget name will appear in UI
    __('Categorías Podcast', 'wpb_widget_domain_06'), 
      
    // Widget description
    array( 'description' => __( 'Widget para mostrar categorías podcast', 'wpb_widget_domain_06' ), ) 
    );
    }
      
    // Creating widget front-end
      
    public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
      
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
    echo $args['before_title'] . $title . $args['after_title'];

    $queried_object = get_queried_object();
    $term_id = $queried_object->term_id;
      
    // This is where you run the code and display the output
 
    $terms = get_terms(
      array(
        'taxonomy' => 'categoria-podcast',
        'number' => 3,
        'exclude' => array($term_id)
        
    ));
    
    $string = '<div class="grid-categories widget-podcast-cat">';
    foreach ($terms as $term) {
      $string .= '<div class="item-cat position-relative mb-3">';
      $string .= '<img src='.z_taxonomy_image_url($term->term_id, "medium").' class="img-cat">';
      $string .= '<a href='.get_term_link($term->name, "categoria-podcast").' class="cat-caption d-flex justify-content-center align-items-center flex-column position-absolute">';
      $string .= '<img src="'.get_stylesheet_directory_uri().'/assets/img/podcast-resources/sound-waves-1.png" class="position-absolute sound-waves">';
      $string .= '<h5>'.$term->name.'</h5>';
      $string .= '</a>';
      $string .= '</div>';
    }
    $string .= '</div>';
    
    echo $string;

    echo $args['after_widget'];
    }
              
    // Widget Backend 
    public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
    $title = $instance[ 'title' ];
    }
    else {
    $title = __( 'New title', 'wpb_widget_domain_06' );
    }
    // Widget admin form
    ?>
    <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php 
    }
          
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
    }
     
    // Class wpb_widget ends here
    } 
     
 
    // Register and load the widget
    function wpb_load_widget_06() {
        register_widget( 'wpb_widget_06' );
    }
    add_action( 'widgets_init', 'wpb_load_widget_06' );