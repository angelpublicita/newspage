<?php

// Widget Más Noticias
class wpb_widget_07 extends WP_Widget {
  
	function __construct() {
	parent::__construct(
	  
	// Base ID of your widget
	'wpb_widget_07', 
	  
	// Widget name will appear in UI
	__('Más podcast', 'wpb_widget_domain_07'), 
	  
	// Widget description
	array( 'description' => __( 'Widget para mostrar más podcast', 'wpb_widget_domain_07' ), ) 
	);
	}
	  
	// Creating widget front-end
	  
	public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	  
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	if ( ! empty( $title ) )
	echo $args['before_title'] . $title . $args['after_title'];
	  
	// This is where you run the code and display the output
	$id_author .= get_the_author_meta('ID', $author_id);
	$widg = '';
	
	$id_post_curr = get_the_ID();
    
	$post = get_post();
    $exclude = $post->ID;
	$args = array(
		'post_type' => 'podcast',
		'posts_per_page' => 3,
		'post__not_in' => array($id_post_curr)
	);
	$post_query = new WP_Query($args);
		if($post_query->have_posts() ) {
			while($post_query->have_posts() ) {
				$post_query->the_post();
				$limit_title_autor = get_the_title();

				$widg .= '<div class="post-sidebar author-post">';
				$widg .= '<a href="'.get_the_permalink().'" class="link-post-author"></a>';
				$widg .= '<div class="cont-img-post">';
				$widg .= get_the_post_thumbnail($post = null, $size = "post-full", $attr = "class=img-card-post img-fluid");
				$widg .= '</div>';
				$widg .= '<div class="cont-content-post">';
				$widg .= '<h3>'.mb_strimwidth($limit_title_autor, 0, 55, '...').'</h3>';
				$widg .= '<span>ESCUCHAR <i class="fas fa-chevron-right"></i></span>';
				$widg .= '</div>';
				$widg .= '</div>';
			}
		}
	wp_reset_query ();
	echo $widg;
	echo $args['after_widget'];
	}
			  
	// Widget Backend 
	public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
	$title = $instance[ 'title' ];
	}
	else {
	$title = __( 'New title', 'wpb_widget_domain_07' );
	}
	// Widget admin form
	?>
	<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<?php 
	}
		  
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	$instance = array();
	$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	return $instance;
	}
	 
	// Class wpb_widget ends here
} 
	 
 
// Register and load the widget
function wpb_load_widget_07() {
    register_widget( 'wpb_widget_07' );
}
add_action( 'widgets_init', 'wpb_load_widget_07' );