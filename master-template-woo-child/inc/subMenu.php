<?php

add_filter('wp_nav_menu_items', 'do_shortcode');
function submenu_items_func_test($atts){

	// Attributes
	$atts = shortcode_atts(
		array(
			'post-type'	=> 'post',
			'limite_post' => 4,
			'categoria' => '',
		),
		$atts,
			'submenu_items_func_test'
		);
	
	if($atts['post-type'] == 'podcast'){
		$args = array(
			'post_type' => $atts['post-type'],
			'posts_per_page' => $atts['limite_post']
		);
	} else {
		$args = array(
			'post_type' => $atts['post-type'],
			'posts_per_page' => $atts['limite_post'],
			'category_name' => $atts['categoria']
		);
	}
	


	$string = '<div class="art-relacionados">';
	$string .= '<div class="post-rel row">';
	$post_query = new WP_Query($args);
				if($post_query->have_posts() ){
					while($post_query->have_posts() ){
						
						$post_query->the_post();
						$thumbID = get_post_thumbnail_id( $post->ID, 'medium' );
						$imgDestacada = wp_get_attachment_url( $thumbID );
						$string .= '<div class="col-md-3 item">';

						if(get_post_type() == 'podcast'){
							$categories = get_the_terms($post->ID, 'categoria-podcast'); 
							$cat_name = $categories[0]->name;
							$cat_link = get_term_link( $cat_name, 'categoria-podcast');
						} else {
							$categories = get_the_category();
							$lastCat = ($categories);
							$cat_name = $lastCat[0]->name;

							foreach($categories as $category){
								$cat_link = get_category_link($category->cat_ID);
							}
						}
						
						$string .= '<a href="'.$cat_link.'" class="menu-category">'.$cat_name.'</a>';
						$string .= '<a href="'.get_the_permalink().'" class="link-art">';
						$string .= '<div class="img-cont">';
						$string .= '<img src="'.$imgDestacada.'" alt="" width="80px">';
						$string .= '</div>';
						$string .= '<h3 class="title-item">'.get_the_title().'</h3>';
						$string .= '</a>';
						$string .= '</div>';
					}

	}

	$string .= '</div>';
	$string .= '</div>';
	
	wp_reset_query ();

	return $string;
}


add_shortcode('submenu_items_test', 'submenu_items_func_test');