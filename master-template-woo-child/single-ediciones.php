<?php



get_header();



?>



<div class="container edition-post">

    <div class="header">

        <h1 class="title-edition"><?php echo get_the_title(); ?></h1>

        <div class="d-flex flex-row align-items-center justify-content-between share-date-bar">

            <div class="date-post p-2">

                <span class="date"><i class="far fa-clock"></i><?php the_date(); ?></span>

            </div>



            <!--Share Links-->

            <div class="share-links d-flex flex-row align-items-center">

                <span class="text-share mr-2">Compartir en:</span>

                <ul class="nav justify-content-center">

                    <li class="nav-item facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="button-icon" target="_blank"><i class="fab fa-facebook-f"></i></a></li>

                    <li class="nav-item twitter"><a href="https://twitter.com/home?status=<?php the_permalink(); ?>"  class="button-icon" target="_blank"><i class="fab fa-twitter" target="_blank"></i></a></li>

                    <li class="nav-item linkedin"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=" class="button-icon" target="_blank"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>

                    <li class="nav-item mail"><a href="mailto:info@example.com?&subject=&body=<?php the_permalink(); ?> " class="button-icon" target="_blank"><i class="far fa-envelope"></i></a></li>

                </ul>

            </div>

        </div>

    </div>



    <div class="content">

        <?php the_content();?>

    </div>



    <div class="edition-oters">

        <h2 class="title-line-one">OTRAS EDICIONES</h2>

        <div class="loop-edition">

        <?php

            $loop = new WP_Query( array(

                'post_type' => 'Ediciones',

                'posts_per_page' => 8

                )

            );

        ?>



        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

        <a href="<?php the_permalink(); ?>" class="item-edition">

            <?php the_post_thumbnail("medium", array('class' => 'img-card-post img-fluid')); ?>

        </a>  

        <?php endwhile; wp_reset_query(); ?>



        </div>

    </div>

</div>



<?php



get_footer();