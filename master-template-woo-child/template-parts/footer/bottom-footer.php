<?php
/**
 * Template part for displaying bottom footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Master_Template
 */

global $geniorama
?>

<div class="bottom-footer p-2 pt-4">
			<div class="container">
				<div class="row align-items-center">
					<?php $val_ex = $geniorama['opt-multi-select-social-buttons']; foreach($val_ex as $valor_campo): ?>
						<?php if ($valor_campo == '4'): ?>
							<div class="col-12 col-md-4 links-footer text-center">
								<ul class="nav social-nav">
									<?php show_social_buttons('social-nav'); ?>
								</ul>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
						<div class="col-12 col-md-4 text-center">
							<p class="m-0">© Revista La Liga 2020 | Todos los derechos reservados</p>
						</div>

						<div class="col-12 col-md-4 text-center privacity">
							<p class="m-0"><a href="<?php echo home_url(); ?>/politica-privacidad/">Políticas de privacidad</a></p>
						</div>
				</div>
			</div>
		</div>