<?php
 //Template para mostrar post
global $geniorama;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php get_template_part('template-parts/subheader') ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex flex-row align-items-center justify-content-between share-date-bar">
                    <div class="date-post p-2">
                        <?php
                            $terms_post = get_the_terms( $post->id, 'tipo_publicacion' );
                            if ($terms_post) {
                                $terms_slugs_post = array();
                                    
                                foreach( $terms_post as $term_p ) {
                                    $terms_slugs_post[] = $term_p->slug; // save the slugs in an array
                                }
                            
                            if (in_array("columna", $terms_slugs_post)) {
                                    ?>
                                        <span class="author"><i class="fas fa-user-circle"> </i><?php echo get_the_author_meta('display_name', $author_id); ?></span>
                                    <?php
                                }
                            }
                            ?>
                        <?php if(get_field("nombre_autor")): ?>
                            <span class="author"><i class="fas fa-user-circle"> </i><?php the_field("nombre_autor"); ?></span>
                        <?php endif; ?>
                        <span class="date"><i class="far fa-clock"></i><?php the_date(); ?></span>
                    </div>

                     <!--Share Links-->
                     <?php if(get_field('field_5e0f7f166bbf1')): ?>
                        <div class="share-links d-flex flex-row align-items-center">
                            <span class="text-share mr-2">Compartir en:</span>
                            <ul class="nav justify-content-center">
                                <li class="nav-item facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="button-icon" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="nav-item twitter"><a href="https://twitter.com/home?status=<?php the_permalink(); ?>"  class="button-icon" target="_blank"><i class="fab fa-twitter" target="_blank"></i></a></li>
                                <li class="nav-item linkedin"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=" class="button-icon" target="_blank"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                                <li class="nav-item mail"><a href="mailto:info@example.com?&subject=&body=<?php the_permalink(); ?> " class="button-icon" target="_blank"><i class="far fa-envelope"></i></a></li>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <!--end share links-->
                </div>
            </div>
            <div class="col-12 <?php 
                if (get_field('field_5e0f670f0f320')) {
                    echo "col-md-8";
                }
            ?> post-col">
                <div class="post-content">
                    <div class="entry-content">
                        <?php
                        the_content( sprintf(
                            wp_kses(
                                /* translators: %s: Name of current post. Only visible to screen readers */
                                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'master-template-woo' ),
                                array(
                                    'span' => array(
                                        'class' => array(),
                                    ),
                                )
                            ),
                            get_the_title()
                        ) );
                        ?>
                        <!--Share Links-->
                        <div class="post-content p-4 row">
                        <?php 
                            $args = array('post_type' => 'post', 'posts_per_page' => 2,);
                            $post_query = new WP_Query($args);

                            if($post_query->have_posts() ) :
                                while($post_query->have_posts() ) :
                                        $post_query->the_post();
                                        $limit_title_autor = get_the_title();
                                        $thumbID = get_post_thumbnail_id( $post->ID );
                                        $imgDestacada = wp_get_attachment_url( $thumbID );
                        ?>
                    
                        <div class="post-sidebar author-post col-md-5 p-0 mr-3">
                            <a href="<?php echo get_the_permalink(); ?>" class="link-post-author"></a>
                            <div class="cont-img-post">
                             <img src="<?php echo $imgDestacada; ?>" alt="">
                            </div>
                            <div class="cont-content-post">
                                <h3><?php echo mb_strimwidth($limit_title_autor, 0, 55, '...'); ?></h3>
                                <span>LEER MÁS <i class="fas fa-chevron-right"></i></span>
                            </div>
                        </div>
                        <?php
                            wp_reset_query ();
                            endwhile;
                            else: 
                        ?>

                            Oops, there are no posts.

                        <?php
                             
                            endif;
                        ?>

                        </div>
                        <?php
                        $terms = get_the_terms( $post->id, 'tipo_publicacion' );
                        if ($terms) {
                            $terms_slugs = array();
                                
                            foreach( $terms as $term ) {
                                $terms_slugs[] = $term->slug; // save the slugs in an array
                            }
                        
                        if (in_array("columna", $terms_slugs)) {
                                $columna = '';
                                $columna .= '<a href="'.esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ).'">';
                                $columna .= '<span class="author author-link">'.get_avatar( get_the_author_meta( 'ID' ), 80 ).' Por: <span class="name">'.get_the_author_meta('display_name').'</span></span>';
                                $columna .=  '</a>';

                                echo $columna;
                            }
                        }
                        ?>
						
						<div class="text-center ad-cont mt-4 ad-pc" id="anuncio-7">
                        	<?php echo do_shortcode('[adrotate banner="7"]'); ?>
                    	</div>

                        <div class="text-center mt-0 ad-mobile ad-post ad-cont">
                        	<?php echo do_shortcode('[adrotate banner="12"]'); ?>
                    	</div>

                        <?php echo do_shortcode('[gs-fb-comments]') ?>
                        <?php if(get_field('field_5e0f7f166bbf1')): ?>
                            <div class="share-date-bar mt-5 bar-second">
                                <div class="share-links d-flex align-items-center">
                                    <span class="text-share mr-2">Compartir en:</span>
                                    <ul class="nav">
                                        <li class="nav-item facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="button-icon" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li class="nav-item twitter"><a href="https://twitter.com/home?status=<?php the_permalink(); ?>"  class="button-icon" target="_blank"><i class="fab fa-twitter" target="_blank"></i></a></li>
                                        <li class="nav-item linkedin"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=" class="button-icon" target="_blank"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                                        <li class="nav-item mail"><a href="mailto:info@example.com?&subject=&body=<?php the_permalink(); ?> " class="button-icon" target="_blank"><i class="far fa-envelope"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!--end share links-->
                        <?php
                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'master-template-woo' ),
                            'after'  => '</div>',
                        ) );
                        ?>

                        <?php

                            if ($geniorama['on-off-links-post']) {
                                if ($geniorama['opt-text-links-post'] == 1) {
                                    the_post_navigation(array(
                                        'prev_text'                  => __( '%title' ),
                                        'next_text'                  => __( '%title' ),
                                        'screen_reader_text' => __( 'Continuar leyendo', 'master-template-woo' )
                                    ));
                                } else {
                                    the_post_navigation(array(
                                        'prev_text'                  => __($geniorama['prev-title-post']),
                                        'next_text'                  => __($geniorama['next-title-post']),
                                        'screen_reader_text' => __( 'Continuar leyendo', 'master-template-woo' )
                                    ));
                                }
                            }
                        ?>
                    </div><!-- .entry-content -->

                    <!-- Artículos relacionados -->
                    <div class="art-relacionados">
                        <h2>ARTÍCULOS RELACIONADOS</h2>
                        <div class="post-rel row">
                            
                        <?php 
                            $tags_post = get_the_tags($post->id);

                            if(!$tags_post){
                                $tags_loop = '';
                            
                                foreach ($tags_post as $tag) {
                                    $tags = $tag->name;
                                    $tags_loop .=  $tags . ',';
                                }
								$id = get_the_ID();
                                $args = array('post_type' => 'post', 'posts_per_page' => 3, 'tag' => $tags_loop, 'post__not_in' => array($id));
                            } else {
								$id = get_the_ID();
                                $cat = get_the_category();
                                $cat =  $cat[0]->cat_name;
                                $args = array('post_type' => 'post', 'posts_per_page' => 3, 'category_name' => $cat, 'post__not_in' => array($id));
                            }
                            
                            $post_query = new WP_Query($args);
                            if($post_query->have_posts() ) :
                                while($post_query->have_posts() ) :
                                        $post_query->the_post();
                                        $thumbID = get_post_thumbnail_id( $post->ID );
                                        $imgDestacada = wp_get_attachment_url( $thumbID, 'thumbnail' );
                        ?>
                        <div class="col-md-4">
                            <a href="<?php echo get_the_permalink(); ?>" class="link-art">
                                <div class="img-cont">
                                    <img src="<?php echo $imgDestacada; ?>" alt="">
                                </div>
                                <h3><?php echo get_the_title(); ?></h3>
                            </a>
                        </div>
                        <?php
                            wp_reset_query ();
                            endwhile;
                            endif;
                        ?>

                        </div>
                    </div>
                    
                </div>
                
				<div class="text-center mt-4 mb-5 ad-pc ad-cont">
                	<?php echo do_shortcode('[adrotate banner="27"]'); ?>
                </div>
				
				<div class="text-center mt-4 mb-5 ad-cont ad-pc" id="anuncio-10">
                	<?php echo do_shortcode('[adrotate banner="18"]'); ?>
                </div>
				
				<div class="text-center mt-4 mb-5 ad-mobile ad-cont">
                	<?php echo do_shortcode('[adrotate banner="28"]'); ?>
                </div>
				
				<div class="text-center mt-4 mb-5 ad-mobile ad-cont">
                	<?php echo do_shortcode('[adrotate banner="15"]'); ?>
                </div>
                
                
            </div>
            <?php if(get_field('field_5e0f670f0f320')): ?>
                <div class="col-12 col-md-4 sidebar-col">
                    <div class="sidebar-single">
                    <?php 
                            $terms = get_the_terms( $post->id, 'tipo_publicacion' ); // get an array of all the terms as objects.
                                if ($terms) {
                                    $terms_slugs = array();
                                
                                    foreach( $terms as $term ) {
                                        $terms_slugs[] = $term->slug; // save the slugs in an array
                                    }
        
                                    if (in_array("noticia", $terms_slugs)) {
                                        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar Noticias')): endif;
                                    } elseif (in_array("columna", $terms_slugs)){
                                        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar Columnas')): endif;
                                    } elseif(in_array("articulo", $terms_slugs)){
                                        get_sidebar();
                                    } else {
                                        get_sidebar();
                                }
                            } else {
                                get_sidebar();
                        }
                    ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</article>