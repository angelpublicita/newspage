<?php
/**
 * Template part for displaying page content in header
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Master_Template
 */

global $geniorama;

?>

<!-- 
*******************************Header Logo Left**********************
-->

    <!--Static Header -->
    <div class="bottom-header static-header type-logo-left">
        <div class="<?php select_width($geniorama['button-set-multi-header-bottom']); ?>">
            <!-- Header mobile -->
		    <?php get_template_part('template-parts/header/header-mobile'); ?>

            <div class="box-logo-and-nav hide-on-mobile">
                <div class="box-logo">
                    <a href="<?php echo get_home_url(); ?>"><?php echo show_logo($geniorama['opt-logo-select']); ?></a>
                </div>

                <div class="box-nav-and-buttons align-items-center">
                    <?php get_template_part('template-parts/menus/menu-principal'); ?>

                    <div class="box-buttons">
                        <ul class="nav align-items-center">
                            <?php echo do_shortcode("[show_social_buttons]"); ?>
                            <li class="nav-item"><a href="#" class="nav-link button-icon"  data-toggle="modal" data-target="#modalSearch"><i class="fas fa-search"></i></a></li>
                        </ul>
                    </div>
                    <?php if($geniorama['wc-show-menu-buttons'] || $geniorama['menu_lateral']): ?>
                        <div class="box-buttons">
                            <?php 
                                if($geniorama['wc-show-menu-buttons']){
                                    get_template_part('template-parts/buttons/buttons-menu-wc');
                                }
                            ?>

                            <?php 
                                if ($geniorama['menu_lateral']){
                                    get_template_part('template-parts/buttons/button-menu-lateral');
                                }
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
                
            </div>
        </div>
    </div>


    <?php if($geniorama['on-off-sticky-header']): ?>
    <!--Sticky Header -->

    <div class="bottom-header sticky-header type-logo-left">
        <div class="<?php select_width($geniorama['button-set-multi-header-bottom']); ?>">
            <!-- Header mobile -->
		    <?php get_template_part('template-parts/header/header-mobile'); ?>

            <div class="box-logo-and-nav hide-on-mobile align-items-center">
                <div class="box-logo">
                    <a href="<?php echo get_home_url(); ?>"><?php echo show_logo($geniorama['opt-logo-sticky-select']); ?></a>
                </div>
                
                <div class="box-nav-and-buttons align-items-center">
                    <?php get_template_part('template-parts/menus/menu-principal'); ?>
                    
                        <div class="box-buttons">
                            <ul class="nav align-items-center">
                                <?php echo do_shortcode("[show_social_buttons]"); ?>
                                <li class="nav-item"><a href="#" class="nav-link button-icon"  data-toggle="modal" data-target="#modalSearch"><i class="fas fa-search"></i></a></li>
                            </ul>
                        </div>
                    
                </div>
                
            </div>
        </div>
    </div>
    <?php endif; ?>