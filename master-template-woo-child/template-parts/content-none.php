<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Master_Template_Woo
 */

?>
<div class="container">
<div class="col-12 mb-5">
		<h1 class="page-title"><?php esc_html_e( 'No hay resultados', 'master-template-woo' ); ?></h1>

	

			<p><?php esc_html_e( 'Lo sentimos, pero nada coincide con sus términos de búsqueda. Intente nuevamente con algunas palabras clave diferentes.', 'master-template-woo' ); ?></p>
		

	</div><!-- .page-content -->
</div><!-- .no-results -->
