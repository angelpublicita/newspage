<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Master_Template_Woo
 */

?>

<div class="col-12 col-md-6">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="target-grid">

                        <div class="header">
                            <?php 
                            $categories = get_the_category(); 
			                $cat_name = $categories[0]->cat_name;
			                $category_link = reset(get_the_category($post->ID));
			                $category_id = $category_link->cat_ID;
                            ?>
                            
                            <a href="<?php echo get_category_link( $category_id ); ?>" class="category"><?php echo $cat_name; ?>
                            </a>

                            <a href="<?php  echo get_the_permalink(); ?>" class="date"><i class="fas fa-clock"></i> <?php the_date(); ?></a>

                        </div>

                        <a href="<?php  echo get_the_permalink(); ?>" class="link-target"></a>

                        <div class="img-target">

                            <?php master_template_woo_post_thumbnail(); ?>
                        </div>

                        <div class="content">

                            <h4 class="title-target"><?php $limit_title = get_the_title(); echo mb_strimwidth($limit_title, 0, 40, '...'); ?>.</h4>

                            <p class="description"><?php $limit_content = get_the_excerpt(); echo mb_strimwidth($limit_content, 0, 55, '...'); ?>.</p>

                        </div>

                        <a href="#" class="icon">

                            <i class="fas fa-chevron-right"></i>

                        </a>

    </div>
    
</article><!-- #post-<?php the_ID(); ?> -->
</div>
