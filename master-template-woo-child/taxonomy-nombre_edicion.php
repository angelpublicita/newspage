<?php

get_header();

?>

 

<div class="entry-category">

    <div class="container">

        <h1><?php the_archive_title(); ?></h1>

        <hr class="divider-subheader">

        <?php if($geniorama['breadcrumbs-on-off']): ?>

                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">

                    <?php

                        if (function_exists('bcn_display')) {

                            bcn_display();

                        }

                    ?>

                </div>

        <?php endif; ?>

    </div>

</div>

 

<section class="content-category">

    <div class="container">

        <div class="row">

                <!-- ESPACIO PARA BARRA DE FILTROS -->

            <div class="col-12 col-md-8">

           
                <?php

                    // CAPTURANDO LOS VALORES POR GET

                    

                     if ( $_GET['orderby']) {

                        $orderby = $_GET['orderby'];

                     } else {

                         $orderby = 'date';

                     }

 

                     if ($_GET['order']) {

                        $order = $_GET['order'];

                     } else {

                        $order = "DESC";

                     }
                     
                    $id_tax = (get_queried_object_id());
 

                     $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                     $wp_query = new WP_Query(

                       array(
                           
                         'post_type' => 'Ediciones',  

                         'orderby' => $orderby,

                         'order' => $order,

                         'cat' => get_query_var('cat'),

                         'paged' => $paged,
                         
                         'tax_query' => array(
                             
                              array(
                                  
                              'taxonomy' => 'nombre_edicion',
                              
                              'terms'    =>  $id_tax,
                               
                              ),
                              
                          ),

                       )

                     );

                ?>
				
                <div class='post-filters-cont'>
                    <div class="results">
                        <span>Mostrando 
                        <?php
                        
                        global $wp_query;
                        $number_post = $wp_query->found_posts;

                        $number_page = $wp_query->post_count;
                        if ($number_post > 7){
                            echo $number_page . ' de ' . $number_post . ' resultados';
                        }else{
                            echo ' ' .$number_post . ' resultados';
                        }
                        ?>
                        </span>
                    </div>
                    <form class='post-filters'>
                        <select name="orderby">
                            <option value='date'>Ordenar por fecha</option>
                            <option value='title'>Order por titulo</option>
                            <option value='rand'>Orden aleatorio</option>
                        </select>
                        <select name="order">
                            <option value='DESC'>Descendente</option>
                            <option value='ASC'>Ascendente</option>
                        </select>
                        <button type="submit" class="btn-filter"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                
                <div class="row">
                <?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>

                    <div class="taxonomy-edition col-md-4">
                        
                        <div class="cont">

                        <a href="<?php the_permalink(); ?>" class="link-target"></a>

                        <div class="img-target">

                            <?php the_post_thumbnail("medium", array('class' => 'img-card-post img-fluid')); ?>

                        </div>

                        <div class="content">

                            <h4 class="title-target"><?php echo get_the_title(); ?></h4>

                        </div>
                        
                        </div>

                    </div>

                <?php endwhile; endif; ?>

                </div>

                <div class="pagination-custom">

                    <?php pagination_custom(); ?>

                </div>

            </div>

            <div class="col-12 col-md-4">

                <div class="sidebar-categories">

                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar Categorías')): endif; ?>

                </div>

            </div>

        </div>

    </div>

</section>

 

 

<?php

get_footer();